package com.example.Person_api;

import java.util.Objects;

public class Person {

private String name;
private String role;
private String skill;

    Person() {}

    Person(String name, String role) {

        this.name = name;
        this.role = role;
    }

    public String getSkill() {
        return this.skill;
    }

    public String getName() {
        return this.name;
    }

    public String getRole() {
        return this.role;
    }

    public void setSkill(Long id) {
        this.skill = skill;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (!(o instanceof Person))
            return false;
        Person employee = (Person) o;
        return Objects.equals(this.skill, employee.skill) && Objects.equals(this.name, employee.name)
                && Objects.equals(this.role, employee.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.skill, this.name, this.role);
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + this.skill + ", name='" + this.name + '\'' + ", role='" + this.role + '\'' + '}';
    }
}

