package com.example.Person_api;

class PersonNotFoundException extends RuntimeException {

    PersonNotFoundException(Long id) {
        super("Could not find employee " + id);
    }
}

