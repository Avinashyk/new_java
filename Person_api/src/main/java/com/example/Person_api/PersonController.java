package com.example.Person_api;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

// tag::hateoas-imports[]
// end::hateoas-imports[]

@RestController
class PersonController {

    private final PersonRepository repository;

    PersonController(PersonRepository repository) {
        this.repository = repository;
    }

    // Aggregate root

    // tag::get-aggregate-root[]
    @GetMapping("/persons")
    CollectionModel<EntityModel<Person>> all() {

        List<EntityModel<Person>> person = repository.findAll().stream()
                .map(person -> EntityModel.of(person,
                        linkTo(methodOn(PersonController.class).one(Person.getSkill())).withSelfRel(),
                        linkTo(methodOn(PersonController.class).all()).withRel("persons")))
                .collect(Collectors.toList());

        return CollectionModel.of(person, linkTo(methodOn(PersonController.class).all()).withSelfRel());
    }
    // end::get-aggregate-root[]

    @PostMapping("/persons")
    Person newPerson(@RequestBody Person newPerson) {
        return repository.save(newPerson);
    }

    // Single item

    // tag::get-single-item[]
    @GetMapping("/employees/{id}")
    EntityModel<Person> one(@PathVariable Long id) {

        Person employee = repository.findBySkill(skill) //
                .orElseThrow(() -> new PersonNotFoundException(id));

        return EntityModel.of(employee, //
                linkTo(methodOn(EmployeeController.class).one(id)).withSelfRel(),
                linkTo(methodOn(EmployeeController.class).all()).withRel("persons"));
    }
    // end::get-single-item[]

    @PutMapping("/employees/{id}")
    Person replaceEmployee(@RequestBody Person newEmployee, @PathVariable Long id) {

        return repository.findBySkill(skill) //
                .map(employee -> {
                    employee.setName(newPerson.getName());
                    employee.setRole(newEmployee.getRole());
                    return repository.save(employee);
                }) //
                .orElseGet(() -> {
                    newEmployee.setId(id);
                    return repository.save(newEmployee);
                });
    }

    @DeleteMapping("/employees/{id}")
    void deleteEmployee(@PathVariable Long id) {
        repository.deleteById(id);
    }
}

