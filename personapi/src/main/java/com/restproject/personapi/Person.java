package com.restproject.personapi;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
class Person {
	
	private @Id @GeneratedValue Long id;
	private String name;
	private String role;
	private String skill;
	
	Person(){}
	Person(String name, String role, String skill) {
		this.name = name;
		this.role = role;
		this.skill = skill;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getSkill() {
		return skill;
	}
	public void setSkill(String skill) {
		this.skill = skill;
	}	

}
