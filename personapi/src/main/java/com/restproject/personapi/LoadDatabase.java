package com.restproject.personapi;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class LoadDatabase {

	@Bean
	CommandLineRunner initDatabase(PersonRepository repository) {

		return args -> {
			repository.save(new Person("Amit", "Tester","Java"));
			repository.save(new Person("Rahul", "Architect", "API"));
		};
	}

}
