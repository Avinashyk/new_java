package com.restproject.personapi;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class PersonController {

	private final PersonRepository repository;

	PersonController(PersonRepository repository) {
		this.repository = repository;
	}

	// Aggregate root

	// tag::get-aggregate-root[]
	@GetMapping("/persons")
	CollectionModel<EntityModel<Person>> all() {

		List<EntityModel<Person>> persons = repository.findAll().stream()
				.map(person -> EntityModel.of(person))
				.collect(Collectors.toList());

		return CollectionModel.of(persons);
	}
	
	@GetMapping("/persons/{id}")
	EntityModel<Person> one(@PathVariable Long id) {

		Person person = repository.findById(id).orElseThrow(() -> new PersonNotFoundException(id));

		return EntityModel.of(person);
	}
	
	@PostMapping("/persons")
	Person newPerson(@RequestBody Person newPerson) {
		return repository.save(newPerson);
	}
	

}
