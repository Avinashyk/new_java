package com.restproject.personapi;

public class PersonNotFoundException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	PersonNotFoundException(Long id) {
		super("Could not find person " + id);
	}

}
