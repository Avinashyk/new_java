import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @org.junit.jupiter.api.Test
    void add() {
        Calculator calculator= new Calculator();
        int sum=calculator.add(2,6);
        assertEquals(8,sum);
    }

    @org.junit.jupiter.api.Test
    void subtract() {
        Calculator calculator= new Calculator();
        int subtract=calculator.subtract(8,6);
        assertEquals(2,subtract);
    }

    @org.junit.jupiter.api.Test
    void multiply(){
        Calculator calculator = new Calculator();
        int multiply=calculator.multiply(4,4);
        assertEquals(16, multiply);
    }
    @org.junit.jupiter.api.Test
    void divide(){
        Calculator calculator = new Calculator();
        double divide=calculator.devide(10,5);
        assertEquals(2,divide);
    }
}