import java.io.*;
import java.util.*;

public class ReadFile {
    private Scanner x;

    public void  openFile (){
        try {
            x = new Scanner(new File("C:\\Java_training\\new_java\\training_attendees.csv"));
        }
        catch (Exception e){
        System.out.println("File not found");
        }
    }

    public void readFile(){
        while(x.hasNext()){
            String a=x.next();
            String b=x.next();
            String c=x.next();

            System.out.printf("%s %s %s\n", a,b,c);
        }
    }

    public void closeFile(){
        x.close();
    }
}
