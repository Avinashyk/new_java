import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PersonMain
{
    public static void main(String[] args) {
        try {
            BufferedReader personDetails = new BufferedReader(new FileReader
                    ("C:\\Java_training\\new_java\\training_attendees.csv"));
            String personLine;
            Person person;

            List<Person> personList = new ArrayList<>();
            while ((personLine = personDetails.readLine()) != null)
            {
                // System.out.println(personLine);
                String[] personFromLine=personLine.split(",");
                person=new Person(personFromLine[0],personFromLine[1],personFromLine[2]);
                personList.add(person);
            }
            System.out.println("Number of odi emp is: "+Person.getOdiSkillCount(personList));
            System.out.println("Number of Java emp is: "+Person.getJavaSkillCount(personList));
            System.out.println("Number of Selenium emp is: "+Person.getSeleniumSkillCount(personList));
            System.out.println("Roles are: "+Person.getAllRoleCount(personList));
           // System.out.println(Person.getResult());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    }
