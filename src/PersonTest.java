import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {

    @Test
    void getOdiSkillCountTest() {
        Person personTest=new Person("avinash","developer","odi");
        Person personTest2=new Person("hemanth","developer","odi");
        Person personTest3=new Person("ramesh","developer","java");
        List<Person> personList=new ArrayList<Person>();
        personList.add(personTest);
        personList.add(personTest2);
        personList.add(personTest3);
        long count=Person.getOdiSkillCount(personList);
        assertEquals(2,count);
    }

    @Test
    void getJavaSkillCountTest() {
        Person personTest=new Person("harish","developer","odi");
        Person personTest2=new Person("rajesh","developer","odi");
        Person personTest3=new Person("ramesh","developer","java");
        List<Person> personList=new ArrayList<Person>();
        personList.add(personTest);
        personList.add(personTest2);
        personList.add(personTest3);
        long count=Person.getJavaSkillCount(personList);
        assertEquals(1,count);

    }
    @Test
    void getSeleniumSkillCount(){
        Person personTest=new Person("rama","tester","selenium");
        Person personTest2=new Person("raja","tester","java");
        List<Person> personList= new ArrayList<Person>();
        personList.add(personTest);
        personList.add(personTest2);
        long count=Person.getSeleniumSkillCount(personList);
        assertEquals(1,count);

    }
    @Test
    void getAllRoleCount(){
        //adding input data
        HashSet<String> test= new HashSet<String>();
        test.add("tester");
        //creating an object for expected result
        Person personRole= new Person("avi","tester","java");
        List<Person> personList= new ArrayList<Person>();
        personList.add(personRole);
        HashSet<String> count=Person.getAllRoleCount(personList);
        assertEquals(test,count);
    }
}
