package Java_Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ReadMain {

    public static void main(String[] args){
        try {
            BufferedReader personDetails = new BufferedReader(new FileReader("C:\\Java_training\\new_java\\training.csv"));
            String personLine;
            Read person;

            List<Read> personList = new ArrayList<>();
            while ((personLine = personDetails.readLine()) != null)
            {
                // System.out.println(personLine);
                String[] personFromLine=personLine.split(",");
                person=new Read(personFromLine[0],personFromLine[1],personFromLine[2]);
                personList.add(person);
            }
            System.out.println(Read.getOdiSkillCount(personList));
            System.out.println(Read.getJavaSkillCount(personList));
            System.out.println(Read.getSeleniumSkillCount(personList));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
