package Java_Test;

import java.util.List;

public class Read {

    private String name;
    private String role;
    private String skill;

    public Read(String name, String role, String skill) {
        this.name = name;
        this.role = role;
        this.skill = skill;
    }

    public String getName(){
        return name;
    }

    public String getRole(){
        return role;

    }

    public String getSkill(){
        return skill;
    }

    public  void setName(String name){
        this.name=name;
    }

    public void setRole(String role){
        this.role=role;
    }
    public void setSkill(String skill){
        this.skill=skill;
    }
    public static long getOdiSkillCount(List<Read> personList) {
        return personList.stream().filter(kt -> kt.getSkill().equals("odi")).count();
    }

    public static long getJavaSkillCount(List<Read> personList) {
        return personList.stream().filter(s -> s.getSkill().equals("java")).count();
    }

    public static long getSeleniumSkillCount(List<Read> personList) {
        return personList.stream().filter(s -> s.getSkill().equals("selenium")).count();
    }

    public static long getAllSkillsCount(List<Read> personList) {
        //iterate personList
        //create HashSet
        //set.size()
        //output expected=3
        return personList.stream().filter(s -> s.getSkill().equals("selenium")).count();
    }

    public static long getPersonSkillMap(List<Read> personList) {
        //iterate personList
        //create HashMap
        //set.size()
        //Key=personname
        //value=Skill
        //output expected is HashMap of personName and skill
        return personList.stream().filter(s -> s.getSkill().equals("selenium")).count();
    }
}

