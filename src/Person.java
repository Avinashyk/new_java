import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class Person {
    private String name;
    private String role;
    private String skill;

    public Person(String name, String role, String skill) {
        this.name = name;
        this.role = role;
        this.skill = skill;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public static long getOdiSkillCount(List<Person> personList) {
        return personList.stream().filter(s -> s.getSkill().equals("odi")).count();
    }

    public static long getJavaSkillCount(List<Person> personList) {
        return personList.stream().filter(s -> s.getSkill().equals("java")).count();
    }

    public static long getSeleniumSkillCount(List<Person> personList) {
        return personList.stream().filter(s -> s.getSkill().equals("selenium")).count();

    }

    public static HashSet<String> getAllRoleCount(List<Person> personList) {
        HashSet<String> roleCount = new HashSet<String>();
        for (Person pr : personList){
            roleCount.add(pr.getRole());
        }
            return roleCount;
        }

}



